<?php

namespace TomasVodrazka\Forms;

/**
 * Description of FileUtils
 *
 * @author Tomas
 */
class FileUtils {

	/**
	 * 
	 * @param strin $fileName
	 * @return string
	 */
	public static function resolveIcons($fileName) {
		$names = explode('.', $fileName);
		switch ($names[count($names) - 1]) {
			case 'pdf':
				return 'file-pdf-o';
			case 'doc':
			case 'docx':
				return 'file-word-o';
			case 'xls':
			case 'xlsx':
				return 'file-excel-o';
			case 'ppt':
			case 'pptx':
				return 'file-powerpoint-o';
			case 'jpg':
			case 'png':
			case 'gif':
			case 'bmp':
				return 'file-image-o';
			case 'zip':
			case 'rar':
				return 'file-archive-o';
			case 'mp3':
				return 'file-audio-o';

			default:
				return 'file-o';
		}
	}

}
