<?php

namespace TomasVodrazka\Forms\Managers;

/**
 *
 * @author Tomáš
 */
interface IManager {

	public function getById($id);

	public function save($values);

	public function delete($id, $user = null);

	public function getAll();

	public function getArrayForForm();

	public function toArray($rows, $indexed = true);
}
