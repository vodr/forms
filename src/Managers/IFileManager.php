<?php

namespace TomasVodrazka\Forms\Managers;

/**
 *
 * @author Tomas
 */
interface IFileManager extends IManager {

	public function setPaths($filePath, $publicPath);

	public function getByItem($item);

	/**
	 * 
	 * @param int $item
	 * @return array Files devidet to the positions, indexes of array are the positions
	 */
	public function getDevided($item);

	/**
	 * @return string Public path on the internet
	 */
	public function getPublicPath();

	/**
	 * @return array Positions for files
	 */
	public function getPositions();
}
