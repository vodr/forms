<?php

namespace TomasVodrazka\Forms\Managers;

/**
 * Description of AbstractFileDbManager
 *
 * @author Tomas
 */
class AbstractFileDbManager extends AbstractDbManager implements \TomasVodrazka\Forms\Managers\IFileManager {

	protected $filePath;
	protected $publicPath;

	public function setPaths($filePath, $publicPath) {
		$this->filePath = $filePath;
		$this->publicPath = $publicPath;
	}

	public function getByItem($item) {
		
	}

	public function getDevided($item) {
		$rows = array('U' => array(), 'D' => array());
		foreach ($this->getByItem($item) as $row) {
			if ($row['position'] == 'U' || $row['position'] == 'B') {
				$rows['U'][] = $row;
			}
			if ($row['position'] == 'D' || $row['position'] == 'B') {
				$rows['D'][] = $row;
			}
		}
		return $rows;
	}

	public function save($values) {
		$id = null;
		if (isset($values['file'])) {
			$file = $values['file'];
			unset($values['file']);
			$id = parent::save($values);
			if ($file->isOk()) {
				$new = array('id' => $id);
				$new['file'] = $this->saveFile($id, $file);
				parent::save($new);
			}
		} else {
			$id = parent::save($values);
		}
		$row = $this->getById($id);
		if (isset($values['icons']) && empty($values['icons']) && !empty($row['file'])) { // should find icons
			$new = array('id' => $id);
			$new['icons'] = \TomasVodrazka\Forms\FileUtils::resolveIcons($row['file']);
			parent::save($new);
		}
		return $id;
	}

	protected function saveFile($id, $file) {
		$name = $this->createFileName($id, $file->getName());
		$file->move($this->filePath . $name);
		return $name;
	}

	protected function createFileName($id, $fileName) {

		return 'f' . $id . '_' . \Nette\Utils\Strings::webalize($fileName, '.');
	}

	public function getPublicPath() {
		return $this->publicPath;
	}

	public function getPositions() {
		return array('U' => 'nad textem', 'D' => 'pod textem', 'B' => 'oboje');
	}

}
