<?php

namespace TomasVodrazka\Forms\Managers;

/**
 * Description of AbstractDbManager
 *
 * @author Tomáš
 */
abstract class AbstractDbManager implements IManager {

	/** @var \Nette\Database\Context */
	protected $database;

	/**
	 *
	 * @var \Nette\Caching\Cache
	 */
	protected $cache;

	function __construct(\Nette\Database\Context $database, \Nette\Caching\Cache $cache) {
		$this->database = $database;
		$this->cache = $cache;
	}

	protected function useCache() {
		return true;
	}

	public function getTableName() {

	}

	protected function getViewName() {
		return $this->getTableName();
	}

	public function getDefaultOrder() {
		return 'id';
	}

	/**
	 *
	 * @return Nette\Database\Table\Selection
	 */
	protected function getTable($deleted = false) {
		$model = $this->database->table($this->getTableName());
		return $deleted ? $model : $model->where('deleted_by IS NULL');
	}

	/**
	 *
	 * @return Nette\Database\Table\Selection
	 */
	protected function getView($deleted = false) {
		$model = $this->database->table($this->getViewName());
		return $deleted ? $model : $model->where('deleted_by IS NULL');
	}

	public function getAll() {
		return $this->getView()->order($this->getDefaultOrder());
	}

	public function cleanCache() {
		$this->cache->clean(array(
			\Nette\Caching\Cache::TAGS => $this->getCacheTags(),
		));
	}

	public function save($values) {
		if (array_key_exists('id', $values)) {
			$id = $values['id'];
			unset($values['id']);
			$this->database->table($this->getTableName())->get($id)->update($values);
		} else {
			$row = $this->database->table($this->getTableName())->insert($values);
			$id = $row->id;
		}
		$this->cleanCache();
		return $id;
	}

	public function delete($id, $user = null) {
		$row = $this->database->table($this->getTableName())->get($id);
		$row->update(array('deleted_by' => $user == null ? -1 : $user));
		$this->cleanCache();
		return $row;
	}

	public function getById($id) {
		return $this->database->table($this->getTableName())->get($id);
	}

	protected function getCacheTags() {
		return array($this->getTableName());
	}

	protected function cacheTableContent($cacheName, $value) {
		$this->cache->save($cacheName, $value, array(
			\Nette\Caching\Cache::TAGS => $this->getCacheTags()
				)
		);
	}

	/**
	 *
	 * @param array $pictsNames
	 * @param array $values
	 * @return \Nette\Http\FileUpload[]
	 */
	protected function seperatePictures($pictsNames, $values) {
		$picts = array();
		foreach ($pictsNames as $name) {
			if (array_key_exists($name, $values) && $values[$name] instanceof \Nette\Http\FileUpload) {
				$picts[$name] = $values[$name];
				unset($values[$name]);
			}
		}
		return $picts;
	}

	/**
	 *
	 * @param \Nette\Http\FileUpload $upload
	 * @param string $id
	 * @param string $name
	 * @param \TomasVodrazka\Forms\ImageProcessors\IImageProcessor $imageProcessor
	 */
	protected function uploadPicture(\Nette\Http\FileUpload $upload, $id, $name, \TomasVodrazka\Forms\ImageProcessors\IImageProcessor $imageProcessor) {
		if ($upload->isOk()) {
			$image = $upload->toImage();
			$fileName = $imageProcessor->process($image, $id, $name);
			if ($fileName != false) {
				$this->save(array('id' => $id, $name => $fileName));
			}
		}
	}

	public function toArray($rows, $indexed = true) {

		$arr = array();
		foreach ($rows as $value) {
			$arr[] = $value->toArray();
		}
		return $arr;
	}

	public function getArrayForForm() {
		if (!$this->useCache()) { //no-cache
			return $this->constructArrayForForm();
		}
		$cacheName = $this->getTableName() . 'ArrayForForm';
		$arr = $this->cache->load($cacheName);
		if ($arr == null) {
			$arr = $this->constructArrayForForm();
			$this->cacheTableContent($cacheName, $arr);
		}
		return $arr;
	}

	protected function constructArrayForForm() {
		return array();
	}

}
